package com.util;

import java.util.Objects;

public class StringUtil {
    public static boolean hasValues(String... values) {
        if (Objects.isNull(values)) {
            return false;
        }
        if (values.length < 1) {
            return false;
        }
        for (var v : values) {
            if (!hasValue(v)) {
                return false;
            }
        }
        return true;
    }

    public static boolean isEmpty(String... values) {
        return !hasValues(values);
    }

    private static boolean hasValue(String v) {
        return !Objects.isNull(v) && !v.isEmpty();
    }
}
