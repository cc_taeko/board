package com.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateTimeFormatterUtil {
    public static final DateTimeFormatter DEFAULT = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss.SSS");

    public static String parseString(LocalDateTime target) {
        return DEFAULT.format(target);
    }
}
