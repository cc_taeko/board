package com.web;

import com.db.UserDaoImpl;
import com.db.data.Error;
import com.db.data.dto.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Objects;

@WebServlet("/adduser")
public class AddUserServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        String id = req.getParameter(User.Context.ID.name);
        String password = req.getParameter(User.Context.PASSWORD.name);
        String name = req.getParameter(User.Context.NAME.name);
        HttpSession session = req.getSession();
        if (Objects.nonNull(session)) {
            clearError(session);
        }
        // 入力値がすべて存在すれば登録処理
        if (User.hasValueAll(id, password, name)) {
            try {
                new UserDaoImpl().add(User.builder().id(id).password(password).name(name).build());
            } catch (SQLException e) {
                resp.getWriter().println(e.toString());
                return;
            }
            req.getRequestDispatcher("/login.jsp").forward(req, resp);
        } else {
            session.setAttribute(Error.Context.IS_ERROR.name(), true);
            session.setAttribute(Error.Context.VALUE.name(), "<font color=\"red\">入力値が不正です。</font><br>");
            // 入力値が不正だったからもう一度ユーザー登録画面へ繊維
            req.getRequestDispatcher("/login.jsp").forward(req, resp);
        }
    }

    private static void clearError(HttpSession session) {
        session.setAttribute(Error.Context.IS_ERROR.name(), null);
        session.setAttribute(Error.Context.VALUE.name(), null);
    }

}
