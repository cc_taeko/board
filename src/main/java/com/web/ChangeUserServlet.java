package com.web;

import com.db.UserDao;
import com.db.UserDaoImpl;
import com.db.data.dto.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/changeUser")
public class ChangeUserServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.setCharacterEncoding("UTF-8");
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(User.ATTRIBUTE_KEY);
        String changeName = request.getParameter(User.Context.NAME.name());
        try {
            UserDao dao = new UserDaoImpl();
            dao.changeName(user, changeName);
            user = dao.getById(user.getId());
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        session.setAttribute(User.ATTRIBUTE_KEY, user);
        request.getRequestDispatcher("/top").forward(request, response);

    }

}
