package com.web;

import com.db.TopicDaoImpl;
import com.db.data.dto.BoardThread;
import com.db.data.dto.Topic;
import com.db.data.dto.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@WebServlet("/addTopic")
public class AddTopicServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        HttpSession session = req.getSession();
        if (Objects.isNull(session) || Objects.isNull(session.getAttribute(User.ATTRIBUTE_KEY))) {
            req.getRequestDispatcher("/").forward(req, resp);
            return;
        }
        User user = (User) session.getAttribute(User.ATTRIBUTE_KEY);
        Topic.TopicBuilder builder = Topic.builder();
        String value = req.getParameter(Topic.Context.VALUE.name());

        List<Topic> topics = (List<Topic>) session.getAttribute(BoardThread.VIEW_ID);
        try {
            Topic addTopic = builder.value(value)
                    .rowIndex(topics.size() + 1)
                    .userName(user.getId())
                    .threadId(topics.get(0).getThreadId())
                    .publishTime(LocalDateTime.now())
                    .build();
            topics.add(addTopic);
            new TopicDaoImpl().add(addTopic);
        } catch (SQLException e) {
            resp.getWriter().println(e.toString());
            return;
        }
        session.setAttribute(BoardThread.VIEW_ID, topics);
        String url = "/thread?" + BoardThread.Context.ID.name() + "=" + topics.get(0).getThreadId();
        req.getRequestDispatcher(url)
                .forward(req, resp);


    }
}
