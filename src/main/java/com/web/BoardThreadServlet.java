package com.web;

import com.db.BoardThreadDaoImpl;
import com.db.TopicDaoImpl;
import com.db.data.dto.BoardThread;
import com.db.data.dto.Topic;
import com.db.data.dto.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@WebServlet("/board")
public class BoardThreadServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html; charset=UTF-8");
        HttpSession session = req.getSession();
        User user = (User) session.getAttribute(User.ATTRIBUTE_KEY);
        if (Objects.isNull(user)) {
            // セッションがないからRootにリダイレクト
            req.getRequestDispatcher("/root").forward(req, resp);
            return;
        }
        String title = req.getParameter(BoardThread.Context.TITLE.name());
        String topicValue = req.getParameter(Topic.Context.VALUE.name());
        try {
            BoardThread thread = new BoardThreadDaoImpl().add(title);
            new TopicDaoImpl().add(
                    Topic.builder()
                            .rowIndex(1)
                            .userName(user.getId())
                            .threadId(thread.getId())
                            .value(topicValue)
                            .publishTime(LocalDateTime.now())
                            .build()
            );
        } catch (SQLException e) {
            resp.getWriter().println(e.toString());
            return;
        }
        List<BoardThread> threads = new ArrayList<>();
        try {
            threads = new BoardThreadDaoImpl().getAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        req.getServletContext().setAttribute(BoardThread.ATTRIBUTE_KEY,
                threads);
        req.getRequestDispatcher("/top.jsp").forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String a = req.getRequestURI();
        // TODO 掲示板IDを取得
        // TODO 掲示板の中身を表示するViewに遷移
    }

    private static Topic getTopic(HttpServletRequest req) {
        return Topic.builder().build();
    }
}
