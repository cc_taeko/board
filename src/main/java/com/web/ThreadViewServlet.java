package com.web;

import com.db.TopicDaoImpl;
import com.db.data.dto.BoardThread;
import com.db.data.dto.Topic;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet("/thread")
public class ThreadViewServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html; charset=UTF-8");
        int id;
        try {
            String a = req.getParameter(BoardThread.Context.ID.name());
            id = Integer.parseInt(a);
        } catch (NumberFormatException e) {
            req.getRequestDispatcher("/").forward(req, resp);
            return;
        }
        List<Topic> topics;
        try {
            topics = new TopicDaoImpl().getByThreadId(id);
        } catch (SQLException e) {
            resp.getWriter().println(e.toString());
            return;
        }
        HttpSession session = req.getSession();
        session.setAttribute(BoardThread.VIEW_ID, topics);
        req.getRequestDispatcher("board.jsp").forward(req, resp);
    }
}
