package com.web.view;

public interface Message<T> {
    String getMessage();
}
