package com.web;

import com.db.BoardThreadDaoImpl;
import com.db.data.dto.BoardThread;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@WebServlet("/top")
public class TopPageServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // DBからスレッドを取得して格納する
        List<BoardThread> threads = (List<BoardThread>) this.getServletContext().getAttribute(BoardThread.ATTRIBUTE_KEY);
        if (Objects.isNull(threads)) {
            threads = new ArrayList<>();
        }
        try {
            threads = new BoardThreadDaoImpl().getAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        this.getServletContext().setAttribute(BoardThread.ATTRIBUTE_KEY, threads);
        req.getRequestDispatcher("/top.jsp").forward(req, resp);
    }
}
