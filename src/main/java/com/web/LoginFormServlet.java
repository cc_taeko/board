package com.web;

import com.db.UserDaoImpl;
import com.db.data.Error;
import com.db.data.dto.User;
import com.util.StringUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Objects;

@WebServlet("/login")
public class LoginFormServlet extends HttpServlet {

    private static final String ERROR_UNKNOWN_USER = "<font color=\"red\">ユーザーが存在しません。</font><br>";

    private static final String ERROR_NOT_MATCH_PW = "<font color=\"red\">パスワードが一致しません。</font><br>";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = req.getRequestDispatcher("/login.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html; charset=UTF-8");
        String id = req.getParameter(User.Context.ID.name);
        String password = req.getParameter(User.Context.PASSWORD.name);
        if (StringUtil.isEmpty(id, password)) {
            req.getSession().setAttribute(Error.Context.IS_ERROR.name(), true);
            req.getSession().setAttribute(Error.Context.VALUE.name(), "<font color=\"red\">入力情報が不正です。</font><br>");
            req.getRequestDispatcher("/login.jsp").forward(req, resp);
            return;
        }
        try {
            User user = new UserDaoImpl().getById(id);
            // ユーザーが存在しない場合
            if (Objects.isNull(user)) {
                dispatchError(req, resp, ERROR_UNKNOWN_USER);
                return;
            }
            // ユーザーIDとパスワードの検証
            if (user.validatePassword(password)) {
                // ユーザー情報はセッションとして格納
                req.getSession().setAttribute(User.ATTRIBUTE_KEY, user);
                req.getRequestDispatcher("/top").forward(req, resp);
            } else {
                dispatchError(req, resp, ERROR_NOT_MATCH_PW);
            }
        } catch (SQLException e) {
            resp.getWriter().println(e.toString());
        }
    }

    private static void dispatchError(HttpServletRequest req, HttpServletResponse resp, String value) throws ServletException, IOException {
        req.getSession().setAttribute(Error.Context.IS_ERROR.name(), true);
        req.getSession().setAttribute(Error.Context.VALUE.name(), value);
        req.getRequestDispatcher("/login.jsp").forward(req, resp);
    }

}
