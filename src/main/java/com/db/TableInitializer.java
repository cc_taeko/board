package com.db;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Objects;

public class TableInitializer {

    public static void main(String[] args) {
        try (Connection con = DBModule.INSTANCE.getConnection();
             Statement statement = con.createStatement()) {
            StringBuilder builder = new StringBuilder();
            InputStream in = TableInitializer.class.getClassLoader().getResourceAsStream("\\sql\\init.sql");
            Objects.requireNonNull(in);
            try (BufferedReader br = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8))) {
                String line = "";
                while (Objects.nonNull((line = br.readLine()))) {
                    builder.append(line);
                    builder.append("\n");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            for (String query : builder.toString().split(";")) {
                statement.executeUpdate(query);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
