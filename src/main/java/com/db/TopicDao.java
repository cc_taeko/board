package com.db;

import com.db.data.dto.Topic;

import java.sql.SQLException;
import java.util.List;

public interface TopicDao {

    List<Topic> getAll() throws SQLException, ClassNotFoundException;

    List<Topic> getByThreadId(int threadId) throws SQLException, ClassNotFoundException;

    void add(Topic topic) throws SQLException, ClassNotFoundException;
}
