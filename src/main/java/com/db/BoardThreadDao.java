package com.db;

import com.db.data.dto.BoardThread;

import java.sql.SQLException;
import java.util.List;

public interface BoardThreadDao {
    List<BoardThread> getAll() throws SQLException;

    void add(BoardThread thread) throws SQLException;

    BoardThread add(String threadTitle) throws SQLException;

    void addAll(List<BoardThread> thread) throws SQLException;
}
