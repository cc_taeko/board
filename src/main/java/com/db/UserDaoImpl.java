package com.db;

import com.db.data.dto.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl implements UserDao {

    public static final String TABLE_NAME = "USERS";

    @Override
    public List<User> getAll() throws SQLException {
        List<User> users = new ArrayList<>();
        try (Connection connection = DBModule.INSTANCE.getConnection()) {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM " + TABLE_NAME);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                User user = User.builder().id((String) rs.getObject("id"))
                        .name((String) rs.getObject("name"))
                        .password((String) rs.getObject("password")).build();
                users.add(user);
            }
        }
        return users;
    }

    @Override
    public User getById(String id) throws SQLException {
        try (Connection con = DBModule.INSTANCE.getConnection()) {
            PreparedStatement statement = con.prepareStatement("SELECT * FROM " + TABLE_NAME + " WHERE id = ?");
            statement.setString(1, id);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                return User.builder().id((String) rs.getObject("id"))
                        .name((String) rs.getObject("name"))
                        .password((String) rs.getObject("password")).build();
            }
        }
        return null;
    }

    @Override
    public void deleteById(String id) throws SQLException {
        try (Connection con = DBModule.INSTANCE.getConnection()) {

            PreparedStatement statement = con.prepareStatement("DELETE FROM " + TABLE_NAME + " WHERE id = ?");
            statement.setString(1, id);
            statement.executeUpdate();
        }
    }

    @Override
    public void add(User user) throws SQLException {
        try (Connection con = DBModule.INSTANCE.getConnection()) {
            String baseSql = "INSERT INTO " + TABLE_NAME + " values(?,?,?)";
            PreparedStatement statement = con.prepareStatement(baseSql);
            statement.setString(1, user.getId());
            statement.setString(2, user.getName());
            statement.setString(3, user.getPassword());
            statement.executeUpdate();
        }
    }

    @Override
    public void changeName(User user, String newName) throws SQLException {
        try (Connection con = DBModule.INSTANCE.getConnection()) {
            StringBuilder sqlBuilder = new StringBuilder();
            sqlBuilder.append("UPDATE ").append(TABLE_NAME)
                    .append(" SET ").append(User.ColumnName.NAME.name()).append(" = ?")
                    .append(" WHERE ").append(User.ColumnName.ID.name()).append(" = ?");
            PreparedStatement statement = con.prepareStatement(sqlBuilder.toString());
            statement.setString(1, newName);
            statement.setString(2, user.getId());
            statement.executeUpdate();
        }
    }

    private static void deleteAll() throws SQLException {
        try (Connection con = DBModule.INSTANCE.getConnection()) {
            con.prepareStatement("DELETE " + TABLE_NAME).execute();
        }
    }

    public static void main(String[] args) throws SQLException {
        new UserDaoImpl().add(
                User.builder().id(new String("あああ"))
                        .name("ishigaki")
                        .password("test").build()
        );

        System.out.println(new UserDaoImpl().getById("test"));
        new UserDaoImpl().deleteById("test");
    }
}
