package com.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBModule {
    // シングルトン強制
    static final DBModule INSTANCE = new DBModule();
    private static final String URI = "jdbc:h2:./db/board";
    private static final String USER = "sa";
    private static final String PASSWORD = "";


    private DBModule() {
    }

    Connection getConnection() throws SQLException {
        return connect();
    }

    private static Connection connect() throws SQLException {
        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        return DriverManager.getConnection(URI, USER, PASSWORD);
    }
}
