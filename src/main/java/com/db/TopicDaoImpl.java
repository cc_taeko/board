package com.db;

import com.db.data.dto.Topic;
import com.db.data.dto.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TopicDaoImpl implements TopicDao {
    public static final String TABLE_NAME = "TOPICS";

    @Override
    public List<Topic> getAll() throws SQLException {
        if (true) {
            throw new SQLException("未実装");
        }
        // TODO 実装
        try (Connection con = DBModule.INSTANCE.getConnection()) {
        }
        return null;
    }

    @Override
    public List<Topic> getByThreadId(int threadId) throws SQLException {
        List<Topic> topics = new ArrayList<>();
        try (Connection con = DBModule.INSTANCE.getConnection()) {
            PreparedStatement statement = con.prepareStatement(
                    "SELECT * FROM " + TABLE_NAME
                            + " INNER JOIN " + UserDaoImpl.TABLE_NAME
                            + " ON USERS.id = TOPICS.user_id AND "
                            + Topic.ColumnName.THREAD_ID + "= ?");
            statement.setInt(1, threadId);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                topics.add(
                        Topic.builder()
                                .threadId(rs.getInt(Topic.ColumnName.THREAD_ID.name()))
                                .userName(rs.getString(User.ColumnName.NAME.name()))
                                .rowIndex(rs.getInt(Topic.ColumnName.ROW_INDEX.name()))
                                .value(rs.getString(Topic.ColumnName.VALUE.name()))
                                .publishTime((rs.getTimestamp(Topic.ColumnName.PUBLISH_TIME.name()).toLocalDateTime()))
                                .build()
                );
            }
        }
        return topics;
    }

    @Override
    public void add(Topic topic) throws SQLException {
        try (Connection con = DBModule.INSTANCE.getConnection()) {
            PreparedStatement statement = con.prepareStatement("INSERT INTO " + TABLE_NAME + " values(?,?,?,?,?)");
            statement.setInt(1, topic.getRowIndex());
            statement.setString(2, topic.getUserName());
            statement.setInt(3, topic.getThreadId());
            statement.setString(4, topic.getValue());
            statement.setTimestamp(5, Timestamp.valueOf(topic.getPublishTime()));
            statement.execute();
        }
    }

    private static void deleteAll() throws SQLException {
        try (Connection con = DBModule.INSTANCE.getConnection()) {
            con.prepareStatement("DELETE topics").execute();
        }
    }

}
