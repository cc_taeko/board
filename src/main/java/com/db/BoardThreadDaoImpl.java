package com.db;

import com.db.data.dto.BoardThread;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BoardThreadDaoImpl implements BoardThreadDao {

    private static final String TABLE_NAME = "THREADS";

    @Override
    public List<BoardThread> getAll() throws SQLException {
        List<BoardThread> threads = new ArrayList<>();
        try (Connection con = DBModule.INSTANCE.getConnection()) {
            PreparedStatement statement = con.prepareStatement("SELECT * FROM " + TABLE_NAME);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                int threadId = rs.getInt(BoardThread.ColumnName.ID.name());
                var topics = new TopicDaoImpl().getByThreadId(threadId);
                BoardThread.BoardThreadBuilder builder =
                        BoardThread.builder()
                                .id(threadId)
                                .title(rs.getString(BoardThread.ColumnName.TITLE.name()))
                                .topics(topics);
                threads.add(builder.build());
            }
        }
        return threads;
    }

    @Override
    public void add(BoardThread thread) throws SQLException {
        add(thread.getTitle());
    }

    @Override
    public BoardThread add(String threadTitle) throws SQLException {
        BoardThread.BoardThreadBuilder builder = BoardThread.builder();
        try (Connection con = DBModule.INSTANCE.getConnection()) {
            PreparedStatement st = con.prepareStatement("SELECT MAX(" + BoardThread.ColumnName.ID.name() + ") AS LAST_INDEX FROM " + TABLE_NAME);
            ResultSet rs = st.executeQuery();
            int index = 1;
            if (rs.next()) {
                index = rs.getInt("LAST_INDEX");
                index++;
            }
            builder.title(threadTitle).id(index);
            PreparedStatement statement = con.prepareStatement("INSERT INTO " + TABLE_NAME + " values(?,?)");
            statement.setInt(1, index);
            statement.setString(2, threadTitle);
            statement.execute();
        }
        return builder.build();
    }

    @Override
    public void addAll(List<BoardThread> threads) throws SQLException {
        for (var t : threads) {
            add(t);
        }
    }

    private static void deleteAll() throws SQLException {
        try (Connection con = DBModule.INSTANCE.getConnection()) {
            con.prepareStatement("DELETE threads").execute();
        }
    }
}
