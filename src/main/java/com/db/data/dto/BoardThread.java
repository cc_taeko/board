package com.db.data.dto;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class BoardThread {
    public static final String ATTRIBUTE_KEY = "boardThreadObject";
    public static final String VIEW_ID = "threadViewId";

    private int id;
    private String title;
    private List<Topic> topics;

    public enum Context {
        TITLE("board_thread_title"),
        ID("board_thread_id"),
        TOPICS("board_thread_topics");
        private String name;

        private Context(String key) {
            name = key;
        }
    }

    public enum ColumnName {
        ID("id"),
        TITLE("title");
        private final String name;

        private ColumnName(String key) {
            name = key;
        }
    }
}
