package com.db.data.dto;

import lombok.Builder;
import lombok.Value;

import java.time.LocalDateTime;

@Value
@Builder
public class Topic {
    public static final String ATTRIBUTE_KEY = "topicObject";
    private int rowIndex;
    private String userName;
    private int threadId;
    private String value;
    private LocalDateTime publishTime;

    public enum Context {
        VALUE("topic_value");

        private final String name;

        private Context(String key) {
            name = key;
        }
    }

    public enum ColumnName {
        USER_ID("user_id"),
        THREAD_ID("thread_id"),
        VALUE("value"),
        ROW_INDEX("row_index"),
        PUBLISH_TIME("publish_time");

        private String name;

        private ColumnName(String key) {
            name = key;
        }

    }
}
