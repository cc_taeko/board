package com.db.data.dto;

import com.util.StringUtil;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class User {
    public static final String ATTRIBUTE_KEY = "userObject";
    private String id;
    private String name;
    private String password;

    /**
     * パスワード検証
     *
     * @param password パスワード
     * @return 検証結果
     */
    public boolean validatePassword(String password) {
        return this.password.equals(password);
    }

    /**
     * 値が空またはnullではないことを検証する。
     *
     * @param id       　id
     * @param password password
     * @param name     name
     * @return 値がすべてあればtrue
     */
    public static boolean hasValueAll(String id, String password, String name) {
        return StringUtil.hasValues(id, password, name);
    }

    public enum Context {
        ID("user_id"),
        NAME("user_name"),
        PASSWORD("user_password");

        public final String name;

        Context(String key) {
            name = key;
        }
    }

    public enum ColumnName {
        ID("id"),
        PASSWORD("password"),
        NAME("name");
        private final String name;

        private ColumnName(String key) {
            name = key;
        }
    }
}
