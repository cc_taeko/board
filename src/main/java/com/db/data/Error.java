package com.db.data;

import lombok.Value;

@Value
public class Error {
    private boolean isError;
    private String value;

    public enum Context {
        IS_ERROR("error_is_error"),
        VALUE("error_value");

        private final String name;

        Context(String key) {
            name = key;
        }
    }
}
