package com.db;

import com.db.data.dto.User;

import java.sql.SQLException;
import java.util.List;

public interface UserDao {


    List<User> getAll() throws SQLException, ClassNotFoundException;

    User getById(String id) throws SQLException, ClassNotFoundException;

    void deleteById(String id) throws SQLException, ClassNotFoundException;

    void add(User user) throws SQLException, ClassNotFoundException;

    void changeName(User user, String newName) throws SQLException;

}

