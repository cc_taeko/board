<%@ page import="com.db.data.Error" %>
<%@ page import="java.util.Objects" %><%--
  Created by IntelliJ IDEA.
  User: taeko
  Date: 2019/08/03
  Time: 19:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>login</title>
</head>
<body>

<%
    StringBuilder builder = new StringBuilder();
    if (Objects.nonNull(session) && Objects.nonNull(session.getAttribute(Error.Context.IS_ERROR.name()))) {
        boolean isError = (boolean) session.getAttribute(Error.Context.IS_ERROR.name());
        if (isError) {
            builder.append(session.getAttribute(Error.Context.VALUE.name()));
        }
    }
%>
<%= builder.toString()%>

<h2>ログイン</h2>
<form method="POST" action="/login">
    ID : <input type="text" name="user_id"><br>
    PASSWORD : <input type="text" name="user_password"><br>
    <input type="submit" value="ログイン">
</form>
<br>

<h2>ユーザー登録</h2>
<form method="POST" action="/adduser">
    ID : <input type="text" name="user_id"><br>
    PASSWORD : <input type="text" name="user_password"><br>
    名前 : <input type="text" name="user_name"><br>
    <input type="submit" value="登録">
</form>
</body>
</html>
