<%@ page import="com.db.data.dto.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<% User user = (User)session.getAttribute(User.ATTRIBUTE_KEY); %>
<head>
    <title>profile</title>
</head>
<body>
ユーザーID : <%= user.getId() %><br>
ユーザー名 : <%= user.getName() %><br>
<form method="POST" action="/changeUser">
    <textarea name="<%=User.Context.NAME.name()%>"></textarea>
    <input type="submit" value="変更"/>
</form>
<a href="/top">戻る</a><br>

</body>
</html>