<%@ page import="com.db.data.dto.BoardThread" %>
<%@ page import="com.db.data.dto.Topic" %>
<%@ page import="com.util.DateTimeFormatterUtil" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: taeko
  Date: 2019/08/04
  Time: 1:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<%
    List<Topic> topics = (List<Topic>) session.getAttribute(BoardThread.VIEW_ID);
%>
<head>
    <title><%=topics.get(0).getThreadId()%>
    </title>
</head>
<body>
<a href="/top">戻る</a><br>
<%
    for (Topic topic : topics) {
%>
<br>
<b><%=topic.getRowIndex()%>  名前: <%=topic.getUserName()%> <%=DateTimeFormatterUtil.parseString(topic.getPublishTime())%>
    <br></b>
<%=topic.getValue()%><br>

<%
    }
%>

<h3>投稿する</h3>
<form method="POST" action="/addTopic">
    <textarea name="<%=Topic.Context.VALUE.name()%>"></textarea><br>
    <input type="submit" value="投稿"/>
</form>
</body>
</html>
