<%@ page import="com.db.data.dto.BoardThread" %>
<%@ page import="com.db.data.dto.Topic" %>
<%@ page import="com.db.data.dto.User" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Objects" %>
<%--
  Created by IntelliJ IDEA.
  User: taeko
  Date: 2019/08/03
  Time: 17:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>TOP</title>
</head>
<%
    // ユーザーはControllerによってnull安全
    User user = (User) session.getAttribute(User.ATTRIBUTE_KEY);
    // TODO 妥当性
    List<BoardThread> threads = (List<BoardThread>) application.getAttribute(BoardThread.ATTRIBUTE_KEY);
%>
<body>
ユーザー：<a href="/profile.jsp"><%=user.getName()%></a> <a href="/logout">ログアウト</a><br>

<h2>スレッド一覧<br></h2>
<ul>
    <% // スレッドをリスト表示
        if (Objects.nonNull(threads)) {
            for (BoardThread thread : threads) { %>
    <li>
        <a href="/thread?<%=BoardThread.Context.ID.name()+"="+thread.getId()%>"><%=thread.getTitle()%>
            (<%=thread.getTopics().size()%>)
        </a>
    </li>
    <% }
    }
    %>
</ul>

<h2>スレッド新規作成<br></h2>
<form method="POST" action="/board">
    スレッドタイトル : <input type="text" name="<%=BoardThread.Context.TITLE.name()%>"><br>
    <textarea name="<%=Topic.Context.VALUE.name()%>"></textarea><br>
    <input type="submit" value="作成">

</form>
</body>
</html>
