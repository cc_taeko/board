# 実行方法
 
プロジェクトのルートディレクトリでgradlewコマンドを実行する。  
DB初期化コマンド  
`gradlew tableInit`  
サービスの起動    
`gradlew tomcatRun`  
※Java11環境じゃないと動作しないかも。

